import random

from svst.structs import *
from svst.vote import create_vote
from svst.sigpair import create_sigpair, generate_sigpair
from svst.crypto import default_hash


def create_box(n=64):
    partial_box = {
        'votes': [create_vote(random.choice([0,1])) for _ in range(n)],
        'sigpairs': [create_sigpair(b'\x00'*64, b'\x00'*32) for _ in range(n)]
    }
    partial_hash = default_hash(BallotBox.build(partial_box))
    sigpairs = [generate_sigpair(partial_hash) for _ in range(n)]
    partial_box['sigpairs'] = sigpairs
    complete_box = partial_box
    return complete_box