#!/usr/bin/env python3

"""
This program is responsible for validating and downloading pallet headers.

- loop forever
- access database to find all unprocessed nulldatas
- for each nulldata
  - extract header multihash
  - check header size (<200 bytes) against IPFS
  - check signature against central authority pubkey (env VERIFY_PUBKEY)
  - mark as processed in nd_processed always
  - insert into header_data if signature valid
    - if valid also insert pallet into pallet_index
  - in thread request from IPFS and on success add to header_processed
  - pin to IPFS too
"""

import json
import logging
import socket

logging.basicConfig(level=logging.INFO)
logging.warning("~~~~~~~~\n\n### Header Download Starting ###\n\n~~~~~~~~~")

import subprocess
import argparse
import time
import os

import ipfsapi
import bitcoin.base58
import ed25519

import svst.utils as utils

from database import run_query
from ipfs import ipfs, get_pin
from environment import magic_bytes, verify_pubkey_hex, pallet_period
from graceful_shutdown import should_shutdown


MB_LENGTH = len(magic_bytes)


verify_pubkey = ed25519.VerifyingKey(verify_pubkey_hex, encoding='hex')
logging.info("Verification Pubkey: %s" % verify_pubkey_hex)


def get_unprocessed():
    unprocessed = run_query("SELECT id, data "
                            " FROM null_data "
                            " WHERE prefix = %s "
                            " AND id NOT IN (SELECT nd_id FROM nd_processed)"
                            " LIMIT 50"
                            , (magic_bytes,))
    if len(unprocessed) > 0:
        logging.info("%d unprocessed records" % len(unprocessed))
        logging.info(unprocessed[0])
    else:
        logging.info("No unprocessed nulldatas yet")
    return unprocessed


def get_unprocessed_headers():
    unprocessed = run_query("SELECT header_multihash FROM header_data "
                            " WHERE header_multihash not in (SELECT header_multihash FROM header_processed) "
                            " ORDER BY last_processed ASC LIMIT 50")
    if len(unprocessed) > 0:
        logging.info("Got %d unprocessed headers" % len(unprocessed))
    return unprocessed


def mark_processed(nd_id):
    logging.info("Marking processed: %s" % nd_id)
    return run_query("INSERT INTO nd_processed (nd_id) VALUES (%s)", (nd_id,))


def add_to_header_data(header_mh):
    logging.info("Adding header to header_data %s" % header_mh)
    return run_query("INSERT INTO header_data (header_multihash, last_processed) VALUES (%s, %s)", (header_mh, 0))


def add_to_header_processed(header_mh, valid, pallet_mh):
    args = (header_mh, valid, pallet_mh)
    logging.info("Adding header to header_processed (%s, %s, %s) and updating header_data.last_processed" % args)
    run_query("UPDATE header_data SET last_processed = %s WHERE header_multihash = %s", (int(time.time()), header_mh))
    return run_query("INSERT INTO header_processed (header_multihash, valid, pallet_multihash) "
                     " VALUES (%s, %s, %s)", args)


def add_to_pallet_index(pallet_mh):
    logging.info("Adding pallet mh to pallet_index %s" % pallet_mh)
    return run_query("INSERT INTO pallet_index (pallet_multihash, last_processed) VALUES (%s, %s)", (pallet_mh, 0))


TICK_PERIOD = pallet_period  # seconds
while not should_shutdown():
    unprocessed_nd = get_unprocessed()

    # process unprocessed nulldatas
    for nd_id, nd in unprocessed_nd:
        mb = nd[:MB_LENGTH]
        ph_mh_bytes = nd[MB_LENGTH:]
        if len(ph_mh_bytes) != 34:
            logging.info("Nulldata not in required format (id=%s)" % nd_id)
            mark_processed(nd_id)
            continue

        header_mh = bitcoin.base58.encode(ph_mh_bytes)
        ph_mh_stat = ipfs.block_stat(header_mh)
        logging.info(ph_mh_stat)
        # technically headers will be 4 + 34 + 64 bytes = 102 (though IPFS reports 110...)
        if ph_mh_stat['Size'] > 200:  # headers must be under 200 bytes
            logging.info("Nulldata failed size verification")
        else:
            add_to_header_data(header_mh)
        mark_processed(nd_id)

    # process unprocessed headers
    for row in get_unprocessed_headers():
        header_mh = row[0]
        pallet_header = get_pin(header_mh)
        try:
            version = pallet_header[:4]
            pallet_mh_bytes = pallet_header[4:4+34]
            pallet_multihash = bitcoin.base58.encode(pallet_mh_bytes)
            sig = pallet_header[4+34:]
            assert len(version) == 4 and version == b'\x00'*4
            assert len(pallet_mh_bytes) == 34
            assert len(sig) == 64
            verify_pubkey.verify(sig, version + pallet_mh_bytes)  # throws an exception on bad signature
            valid = True
        except Exception as e:
            logging.info("Bad validation of header %s, %s" % (header_mh, e))
            valid = False
            pallet_multihash = b''

        add_to_header_processed(header_mh, valid, pallet_multihash)
        if valid:
            add_to_pallet_index(pallet_multihash)

        pallets = run_query("SELECT * FROM pallet_index")
        logging.info("Pallet index n: %s" % len(pallets))

    time.sleep(TICK_PERIOD)
