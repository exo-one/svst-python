#!/bin/bash

if [[ $VERIFY_SECRET_KEY ]]; then
    echo "Producer starting..."
    ./producer-wrapper.py --secret-key $VERIFY_SECRET_KEY --btc-rpc-user $RPCUSER \
        --btc-rpc-password $RPCPASSWORD --fee-rate 0.0014 --magic-bytes $MAGICBYTES \
        --box-per-pallet $BOX_P_PALLET --pallet-n $NUM_PALLETS --pallet-period $PALLET_PERIOD
else
    echo "Producer doing nothing..."
fi
