//
// UTILS
//


// http://stackoverflow.com/questions/5448545/how-to-retrieve-get-parameters-from-javascript
function getParam(val) {
    var result = undefined,
        tmp = [];
    location.search
        //.replace ( "?", "" )
        // this is better, there might be a question mark inside
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === val) result = decodeURIComponent(tmp[1]);
        });
    return result;
}


//
// DEBUG
//

var __debug__ = getParam('debug') !== undefined;  // just include ?debug=1 in the URL
var debug = {};
debug.scaling = 1.0;

if (__debug__) {
    debug.getinfo = [];
    for(var _i = 0; _i < 144 * debug.scaling; _i++){
        var n_pallets = 70 * debug.scaling
        var votes_p_pallet = 150000 * debug.scaling
        debug.getinfo.push({
            'n_pallets': n_pallets * _i,
            'n_votes': n_pallets * _i * votes_p_pallet,
            'votes_for': n_pallets * _i * votes_p_pallet / 2,
            'votes_against': n_pallets * _i * votes_p_pallet / 2,
        })
    }
}

//
// ANGULAR
//

var app = angular.module('svstApp', ['ngOdometer']);

app.controller('SvstController', function($scope, $rootScope, $log, $http, $window){
    $rootScope._ = _;
    $scope._ = _;
    var ctrl = this;
    ctrl.tab = 'intro';
    ctrl.log = [];  // log for msgs, {'type': 'error', 'msg': 'Test Error'}
    ctrl.tmp = {};

    ctrl.setTab = function(newTab){ ctrl.tab = newTab; }
    ctrl.isTab = function(someTab){ return someTab == ctrl.tab; }

    ctrl.getinfo = {
        'n_pallets': 0,
        'n_votes': 0,
        'votes_for': 0,
        'votes_against': 0,
    }
    ctrl.previous_getinfo = ctrl.getinfo

    ctrl.errorHandler = function(err){
        ctrl.log.unshift({type: "error", msg: err, hide: false});
        $log.log("Error", err);
        $log.log(err);
    }
    ctrl.addMsg = function(msg){
        ctrl.log.unshift({type: "msg", msg: msg, hide: false})
        $log.log("added msg", msg);
    }

    ctrl.gotNewGetInfo = function(data){
        ctrl.previous_getinfo = ctrl.getinfo;
        ctrl.getinfo = data.data;
        console.log("Loaded data: ", data);
    }

    ctrl.loadData = function(){
        if (__debug__){
            ctrl.addMsg("New debug getinfo " + (Date.now() / 1000).toString())
            ctrl.gotNewGetInfo({data: debug.getinfo.shift()})
        } else {
            $http.get('/getinfo').then(ctrl.gotNewGetInfo, ctrl.errorHandler);
        }
        setTimeout(ctrl.loadData, 3000);
        $scope.$apply();
    }
    setTimeout(ctrl.loadData, 1000);
});
