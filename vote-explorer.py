#!/usr/bin/env python3

import logging
logging.basicConfig(level=logging.INFO)

from tornado.platform.asyncio import AsyncIOMainLoop
import asyncio
AsyncIOMainLoop().install()

import socket
import time
import psycopg2

import aiopg
import tornado.ioloop
import tornado.web

from environment import dbhost, dbname, dbuser


PORT = 8683


while True:
    try:
        dbhost_ip = socket.gethostbyname(dbhost)
        sqlconn = aiopg.connect(database=dbname, host=dbhost_ip, user=dbuser)
    except (socket.gaierror) as e:
        logging.error("Unable to connect to Database, sleeping 1s: %s" % e)
        time.sleep(1)
    else:
        break


async def setup_db():
    global pool
    while True:
        try:
            pool = await aiopg.create_pool('dbname=%s host=%s user=%s' % (dbname, dbhost_ip, dbuser))
        except psycopg2.OperationalError as e:
            logging.info("Postgres not available, sleeping 5s")
            await asyncio.sleep(5)
        else:
            break


async def run_query(query, args=tuple()):
    global pool
    async with pool.acquire() as conn:
        async with conn.cursor() as cur:
            await cur.execute(query, args)
            ret = []
            async for row in cur:
                ret.append(row)
    return ret


class BaseHandler(tornado.web.RequestHandler):
    def set_default_headers(self, *args, **kwargs):
        self.set_header(r'Access-Control-Allow-Origin', r'*')
        self.set_header(r'Access-Control-Allow-Headers', r'Content-Type')

    def options(self, *args, **kwargs):
        pass


class GetInfoHandler(BaseHandler):
    async def get(self, *args, **kwargs):
        state = (await run_query("SELECT * FROM state_cache WHERE id=1"))[0]
        self.write(dict(zip(['id', 'last_processed', 'n_pallets', 'n_votes', 'votes_for', 'votes_against'], state)))


def make_app():
    return tornado.web.Application([
        (r"/getinfo", GetInfoHandler),
        (r"/(.*)", tornado.web.StaticFileHandler, {'path': './static/'}),
    ])


if __name__ == "__main__":
    app = make_app()
    app.listen(PORT, address='0.0.0.0')
    logging.info("Bringing vote explorer up on port %d" % PORT)
    asyncio.get_event_loop().run_until_complete(setup_db())
    logging.info("DB Connection set up")
    asyncio.get_event_loop().run_forever()


