#!/usr/bin/env python3

"""
This program is responsible for calculating state.

- loop forever
- count up n_votes, n_votes_for, n_votes_against
- cache in id=1 row of state_cache
- sleep
"""

import logging

# Initialise logging and print a header
logging.basicConfig(level=logging.INFO)
logging.warning("~~~~~~~~\n\n### State Maker Starting ###\n\n~~~~~~~~~")

import time

from database import get_state, get_n_valid_pallets, update_state
from graceful_shutdown import should_shutdown
from environment import pallet_period


def should_update_state():
    last_state = get_state()
    n_valid_pallets = get_n_valid_pallets()
    return last_state.n_pallets != n_valid_pallets


TICK_PERIOD = pallet_period  # seconds
while not should_shutdown():
    # Get a list of pallets from the pallets_downloaded table:
    if should_update_state():
        logging.info("Updating state %s", time.time())
        update_state()
        logging.info(" State updated %s", time.time())
    time.sleep(TICK_PERIOD)
