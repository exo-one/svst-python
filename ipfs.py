import ipfsapi
import logging
import time


while True:
    try:
        ipfs = ipfsapi.Client(host="ipfs")
    except ipfsapi.exceptions.ConnectionError as e:
        logging.warning("Unable to connect to IPFS, sleeping 5: %s" % e)
        time.sleep(5)
    else:
        break


def get_pin(multihash):
    logging.info("Retrieving file from IPFS: %s" % multihash)
    file_bytes = ipfs.cat(multihash)
    logging.info("Got file of length %d" % len(file_bytes))
    ipfs.pin_add(multihash)
    logging.info("Pinned %s" % multihash)
    return file_bytes
