#!/usr/bin/env python3

"""
This program is responsible for downloading pallets.

- loop forever
- access database to find all undownloaded pallets
- for each pallet from pallet_index
  - in a new thread:
  - request pallet from IPFS
  - pin pallet to IPFS
  - once downloaded mark in pallet_downloaded
"""

import logging
logging.basicConfig(level=logging.INFO)
logging.warning("~~~~~~~~\n\n### Pallet Download Starting ###\n\n~~~~~~~~~")

import time
from database import run_query
from ipfs import get_pin
from environment import pallet_period
from graceful_shutdown import should_shutdown


def get_pallets_not_downloaded():
    """ Find all pallets that have not been downloaded"""
    not_downloaded = run_query(" SELECT pallet_multihash FROM pallet_index"
                               " WHERE pallet_multihash NOT IN (SELECT pallet_multihash from pallet_downloaded)")
    logging.info("%d pallets that have not been downloaded", len(not_downloaded))
    return not_downloaded


def mark_downloaded(pallet_multihash):
    """ Mark pallets that have been downloaded """
    logging.info("Marking downloaded: %s", pallet_multihash)
    return run_query("INSERT INTO pallet_downloaded (pallet_multihash) VALUES (%s)", (pallet_multihash,))


TICK_PERIOD = pallet_period  # seconds
while not should_shutdown():
    for row in get_pallets_not_downloaded():
        pallet_mh = row[0]
        try:
            get_pin(pallet_mh)
            valid = True
        except Exception as error:
            logging.info("Unable to download pallet %s, %s", pallet_mh, error)
            valid = False
        if valid:
            mark_downloaded(pallet_mh)

    time.sleep(TICK_PERIOD)
