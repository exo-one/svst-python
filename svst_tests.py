from log import logging

import datetime
import requests
import time
import json
import socket
import sys, os
from pprint import pformat

import psycopg2

import bitcoin
import bitcoin.base58
from bitcoinrpc.authproxy import AuthServiceProxy, EncodeDecimal, JSONRPCException

from fancy_log import fancy_log
from database import *
from bitcoind import bitcoind
from environment import magic_bytes, NUM_PALLETS, pallet_period


# Variables to store test state
last_scrape_n = 0
last_header_n = 0
last_pallet_index_n = 0
last_pallet_downloaded_n = 0
last_pallet_valid_n = 0
last_pallet_invalid_n = 0
last_state = tuple()


def step_00_bitcoind():
    """Generate coins and confirm transactions"""
    bitcoind.generate(128)
    getinfo = bitcoind.getinfo()
    fancy_log("Limited Bitcoin Getinfo", {'blocks': getinfo['blocks'], 'balance': getinfo['balance']})
    return getinfo


def step_10_scraper():
    """Update scraper stats and ensure data 'looks' good"""
    global last_scrape_n
    all_scrapes = run_query("SELECT id, prefix, tx_id, data, block_time, block_hash FROM null_data")
    fancy_log("Last Scrape", [] if len(all_scrapes) == 0 else all_scrapes[-1][:2])
    assert len(all_scrapes) >= last_scrape_n
    last_scrape_n = len(all_scrapes)
    for id, prefix, txid, nd, btime, bhash in all_scrapes:
        assert nd[:len(magic_bytes)] == magic_bytes.encode()
        assert len(nd) == 40


def step_20_pallet_headers():
    """Check pallet headers are downloading"""
    global last_header_n
    all_headers = run_query("SELECT * FROM header_processed")
    logging.info("Got %d headers in header_processed" % len(all_headers))
    fancy_log("Last Header", [] if len(all_headers) == 0 else all_headers[-1])
    for id, header_multihash, valid, pallet_multihash in all_headers:
        assert len(bitcoin.base58.decode(header_multihash)) == 34
        if valid:
            assert len(bitcoin.base58.decode(pallet_multihash)) == 34
        else:
            assert len(pallet_multihash) == 0
    last_header_n = len(run_query("SELECT * FROM header_processed WHERE valid = true"))
    logging.info("Got %d valid headers" % last_header_n)


def step_30_pallet_index():
    """Check pallet index is recieving pallets"""
    global last_pallet_index_n
    all_pallets_in_index = run_query("SELECT * FROM pallet_index")
    last_pallet_index_n = len(all_pallets_in_index)
    logging.info("Got %d pallets in index" % last_pallet_index_n)


def step_35_pallet_download():
    # Check pallet downloads:
    global last_pallet_downloaded_n
    all_pallets_downloaded = run_query("SELECT * FROM pallet_downloaded")
    logging.info("Pallet_downloaded rows: %s", len(all_pallets_downloaded))
    fancy_log("Last Pallet", [] if len(all_pallets_downloaded) == 0 else all_pallets_downloaded[-1])
    # Ensure all downloaded pallets exist in the index
    pallets_absent_from_index = run_query(" SELECT pallet_multihash FROM pallet_downloaded"
                                          " WHERE pallet_multihash NOT IN"
                                          " (SELECT pallet_multihash from pallet_index)")
    assert len(pallets_absent_from_index) == 0
    last_pallet_downloaded_n = len(all_pallets_downloaded)


def step_40_pallet_validation():
    global last_pallet_valid_n, last_pallet_invalid_n
    # test validated pallets
    all_valid_pallets = get_valid_pallets()
    last_pallet_valid_n = len(all_valid_pallets)
    for vp in all_valid_pallets:
        assert len(run_query("SELECT * FROM pallet_index WHERE pallet_multihash = %s", (vp.pallet_multihash,))) == 1
        assert vp.n_votes == vp.votes_for + vp.votes_against
    all_invalid_pallets = get_invalid_pallets()
    last_pallet_invalid_n = len(all_invalid_pallets)
    fancy_log("Last Valid Pallet", [] if len(all_valid_pallets) == 0 else all_valid_pallets[-1])


def step_50_check_state():
    """Just get and print state - assertions at end"""
    global last_state
    state = get_state()
    fancy_log("Current State", state)
    last_state = state


def do_test_round():
    step_00_bitcoind()
    step_10_scraper()
    step_20_pallet_headers()
    step_30_pallet_index()
    step_35_pallet_download()
    step_40_pallet_validation()
    step_50_check_state()


# Run the main loop to produce Bitcoin blocks
ROUND_TIME_SEC = pallet_period
NUM_ROUNDS = max(NUM_PALLETS * 6, 20)
for round_n in range(NUM_ROUNDS):
    r_start = time.time()
    fancy_log("Round %d / %d" % (round_n + 1, NUM_ROUNDS), {'time': time.time(), 'date': repr(datetime.datetime.now())})
    do_test_round()
    time.sleep(max(ROUND_TIME_SEC - time.time() + r_start, 0))

    if last_scrape_n == NUM_PALLETS and last_header_n == NUM_PALLETS and last_pallet_index_n == NUM_PALLETS and \
            last_pallet_downloaded_n == NUM_PALLETS and last_pallet_valid_n == NUM_PALLETS and \
            last_state.n_pallets == NUM_PALLETS:
        logging.info("Bailing out - early mark!")
        break


assert last_scrape_n == NUM_PALLETS
assert last_header_n == NUM_PALLETS
assert last_pallet_index_n == NUM_PALLETS
assert last_pallet_downloaded_n == NUM_PALLETS
assert last_pallet_valid_n == NUM_PALLETS
assert last_state.n_pallets == NUM_PALLETS

assert requests.get("http://vote-explorer:8683/getinfo").status_code == 200
explorer_getinfo = requests.get("http://vote-explorer:8683/getinfo").json()  # ensure this doesn't throw an exception

fancy_log("Vote Explorer GetInfo", explorer_getinfo)
assert explorer_getinfo['n_pallets'] == NUM_PALLETS
assert explorer_getinfo['n_votes'] == last_state.n_votes
assert explorer_getinfo['votes_for'] == last_state.votes_for
assert explorer_getinfo['votes_against'] == last_state.votes_against
