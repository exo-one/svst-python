#!/usr/bin/env python3

import logging
from contextlib import suppress
from pprint import pformat

logging.basicConfig(level=logging.INFO)

import sys
import argparse
import signal
import time
from binascii import unhexlify, hexlify

import psycopg2

from bitcoind import bitcoind, JSONRPCException
from database import insert_nulldata, get_earliest_unscanned_block, update_block, insert_unscanned_block, \
    get_latest_unknown_prevhash, have_block, get_unscanned_heights
from fancy_log import fancy_log
from graceful_shutdown import should_shutdown
from environment import pallet_period, start_block


BLOCKCHAIN_LAG = 3  # scrape up to 3 below the tip


exceptions = list()


def get_block_data_and_insert(block_hash):
    block_data = bitcoind.getblockheader(block_hash)
    if 'previousblockhash' not in block_data:
        block_data['previousblockhash'] = "00"*32
    insert_unscanned_block(block_data['hash'], block_data['previousblockhash'],
                           block_data['height'], block_data['time'])
    return block_data


def rescan_blockchain(min_height=0):
    """We should obtain a full index of blocks from at least the starting height.
    This allows us to detect reorganisations and ensure we've scanned every block
    even if the chain reorganises.

    1. Get top block (check against start_block)
    2. Add block to DB and check previous block"""
    latest_prevhash = bitcoind.getbestblockhash()
    while latest_prevhash is not None and not have_block(latest_prevhash) and latest_prevhash != "00"*32:
        block_data = get_block_data_and_insert(latest_prevhash)
        latest_prevhash = block_data['previousblockhash']


min_block_height = max(start_block, 0)
block_height = min_block_height
n_blocks_scanned = 0
while not should_shutdown():
    start = time.time()

    # rescan_blockchain(min_block_height)
    try:
        getinfo = bitcoind.getinfo()
    except (BrokenPipeError, JSONRPCException, Exception) as e:
        logging.warning("encountered error from bitcoind: %s", e)
        time.sleep(5)
        continue
    with suppress(Exception):
        list(map(getinfo.__delitem__, ['errors', 'keypoololdest', 'keypoolsize', 'paytxfee', 'proxy', 'relayfee', 'walletversion']))
    fancy_log("Scraper - Blockchain Getinfo", getinfo)
    blockchain_tip_height = getinfo['blocks']

    if blockchain_tip_height - BLOCKCHAIN_LAG + 1 < block_height:
        logging.info("Waiting for more blocks, tip: %s, next_height: %s", blockchain_tip_height, block_height)
    else:
        #to_scrape = get_unscanned_heights()
        to_scrape = list(range(block_height, blockchain_tip_height - BLOCKCHAIN_LAG + 1))
        logging.info("Got %s more heights to scrape", len(to_scrape))
        for height in to_scrape:
            try:
                bd = bitcoind.getnulldatasatheight(height)
            except JSONRPCException as e:
                logging.error("Encountered Bitcoind Error: %s", e)
                continue

            if len(bd['nulldatatxs']) > 0:
                logging.info(pformat(bd))
                for nulldatatx in bd['nulldatatxs']:
                    logging.info(nulldatatx)
                    if len(nulldatatx['nulldatas']) > 0:
                        nulldata = nulldatatx['nulldatas'][0]  # Only 1st nulldata in a TX is valid; also std txs have <=1 nulldatas
                        try:
                            logging.info([nulldata, nulldatatx])
                            insert_nulldata(unhexlify(nulldata), nulldatatx['txid'], bd['time'], bd['hash'])
                        except psycopg2.IntegrityError as e:
                            logging.warning("Recieved integrity error from postgres: %s", e)
                            exceptions.append(e)
            # update_block(bd['hash'], True)
            n_blocks_scanned += 1
            block_height = height + 1  # set next height to scrape
        logging.info("Scraped %s blocks", n_blocks_scanned)

    finish = time.time()
    sleep_for = max(pallet_period - (finish - start), 0)
    logging.info("Sleeping for %s", sleep_for)
    time.sleep(sleep_for)


if len(exceptions) > 0:
    fancy_log("EXCEPTIONS FOUND", exceptions)
    sys.exit(1)


