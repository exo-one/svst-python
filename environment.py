import os

from fancy_log import fancy_log


magic_bytes = os.getenv('MAGICBYTES')
dbname = os.getenv('POSTGRES_DB')
dbhost = os.getenv('POSTGRES_HOST')
dbuser = os.getenv('POSTGRES_USER')
NUM_PALLETS = int(os.getenv('NUM_PALLETS', 20))
rpcuser = os.getenv('RPCUSER')
rpcpassword = os.getenv('RPCPASSWORD')
verify_pubkey_hex = os.getenv('VERIFY_PUBKEY')
pallet_period = float(os.getenv('PALLET_PERIOD'))
start_block = int(os.getenv('START_BLOCK'))

MB_LEN = len(magic_bytes)
