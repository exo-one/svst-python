import os
import socket
import logging
import time
from collections import namedtuple

import psycopg2

from fancy_log import fancy_log
from environment import dbhost, dbname, dbuser, magic_bytes, MB_LEN


"""
This file contains most of the SQL operations we need to perform.

As a matter of convention:
- Nulldata is stored as raw bytes
- Block hashes are stored hex encoded as varchar(64)s
- TXIDs are stored hex encoded as varchar(64)
"""


def returns_tuple(name, fields, single_row=False):
    """DECORATOR
    This will pass `ret_type` and `field_names` in as keyword args to a db get function.
    `ret_type` is a custom named tuple with fields matching the DB columns

    e.g.
    @returns_tuple("StateCached", ['id', 'last_processed', 'n_pallets', 'n_votes', 'votes_for', 'votes_against'])
    def get_state(ret_type=None, field_names=None):
        ...
    """
    ret_type = namedtuple(name, fields)
    def _decorator(f):
        def _inner(*args, **kwargs):
            ret = f(*args, ret_type=ret_type, field_names=fields, **kwargs)
            # take first row of results if single_row otherwise pass in infinite slice (take the lot)
            return list([ret_type(*row) for row in ret])[(0 if single_row else slice(None, None))]
        return _inner
    return _decorator


while True:
    try:
        dbhost_ip = socket.gethostbyname(dbhost)
        sqlconn = psycopg2.connect(database=dbname, host=dbhost_ip, user=dbuser)
    except (socket.gaierror, psycopg2.OperationalError) as e:
        logging.error("Unable to connect to Database, sleeping 5s: %s" % e)
        time.sleep(5)
    else:
        break


def run_query(sql_query, args=()):
    cur = sqlconn.cursor()
    try:
        resp = cur.execute(sql_query, args)
        logging.debug("Executed:\n%s" % (sql_query, ))
    except psycopg2.ProgrammingError as e:
        fancy_log("SQL Error", e)
        raise e
    if sql_query.strip()[:6] not in ["INSERT", "UPDATE"]:
        try:
            _rows = list(cur.fetchall())
        except psycopg2.ProgrammingError as e:  # probably the case that there's nothing to fetch
            logging.warning("Warning, cur.fetchall() failed for %s", cur.query)
            return list()
        logging.debug("Response has %d rows" % len(_rows))
        rows = [[a if type(a) is not memoryview else bytes(a) for a in row] for row in _rows]
    else:
        rows = []
    sqlconn.commit()
    return list(rows)


def insert_nulldata(nulldata, txid, block_time, block_hash):
    prefix = nulldata[:MB_LEN]
    run_query("INSERT INTO null_data (prefix, tx_id, data, block_time, block_hash) "
              " VALUES (%s, %s, %s, %s, %s) ON CONFLICT (tx_id, block_hash) DO NOTHING",
              (prefix, txid, nulldata, block_time, block_hash))


def get_earliest_unscanned_block():
    ret = run_query("SELECT hash, prevhash, height FROM block_data "
                    " WHERE scanned = false ORDER BY height ASC LIMIT 1")
    if len(ret) == 0:
        return None
    return {'hash': ret[0][0], 'height': ret[0][2], 'prevhash': ret[0][1]}


def get_latest_unknown_prevhash():
    ret = run_query("SELECT prevhash FROM block_data "
                    " WHERE have_prevblock=false"
                    " ORDER BY height DESC LIMIT 1")
    if len(ret) == 0:
        return None
    return ret[0][0]


def update_block(hash, scanned):
    return run_query("UPDATE block_data SET scanned = %s WHERE hash = %s", (scanned, hash))


def insert_unscanned_block(hash, prevhash, height, time):
    run_query("UPDATE block_data SET have_prevblock = true WHERE prevhash = %s", (hash,))
    parent = run_query("SELECT hash FROM block_data WHERE hash = %s", (prevhash,))
    have_prevblock = len(parent) > 0
    return run_query(" INSERT INTO block_data (hash, prevhash, height, time, scanned, have_prevblock)"
                     " VALUES (%s, %s, %s, %s, %s, %s) ON CONFLICT (hash) DO UPDATE "
                     " SET have_prevblock = %s",
                     (hash, prevhash, height, time, False, have_prevblock, have_prevblock))


def have_block(block_hash):
    ret = run_query("SELECT * FROM block_data WHERE hash = %s", (block_hash,))
    return len(ret) > 0


def get_unscanned_heights():
    ret = run_query("SELECT height FROM block_data WHERE scanned = false ORDER BY height ASC LIMIT 50")
    return list([hs[0] for hs in ret])


def get_pallets_downloaded_needing_validation():
    """ Find all pallets that have been downloaded """
    pallets_downloaded = run_query(" SELECT pallet_multihash FROM pallet_downloaded"
                                   " WHERE pallet_multihash NOT IN"
                                   " (SELECT pallet_multihash from pallet_validated)")
    logging.info("%d pallets that have not been downloaded", len(pallets_downloaded))
    return pallets_downloaded


def add_to_pallet_validated(pallet_multihash, valid, n_votes, votes_for, votes_against):
    args = (pallet_multihash, valid, n_votes, votes_for, votes_against)
    logging.info("Adding validated pallet data to pallet_validated: %s", pallet_multihash)
    return run_query(" INSERT INTO pallet_validated (pallet_multihash, valid, n_votes, votes_for, votes_against)"
                     " VALUES (%s, %s, %s, %s, %s)", args)


@returns_tuple("Pallet", ["id", "pallet_multihash", 'valid', 'n_votes', 'votes_for', 'votes_against'])
def get_valid_pallets(ret_type, field_names):
    return run_query("SELECT {fields} FROM pallet_validated WHERE valid = true".format(fields=', '.join(field_names)))


@returns_tuple("Pallet", ["id", "pallet_multihash", 'valid', 'n_votes', 'votes_for', 'votes_against'])
def get_invalid_pallets(ret_type, field_names):
    return run_query("SELECT {fields} FROM pallet_validated WHERE valid = false".format(fields=', '.join(field_names)))


@returns_tuple("State", ['id', 'last_processed', 'n_pallets', 'n_votes', 'votes_for', 'votes_against'], single_row=True)
def get_state(ret_type=None, field_names=None):
    ret = []
    while len(ret) == 0:
        ret = run_query("SELECT {fields} FROM state_cache WHERE id=1".format(fields=', '.join(field_names)))
        if len(ret) == 0:
            update_state()
    return ret


def get_n_valid_pallets():
    return run_query("SELECT COUNT(id) FROM pallet_validated WHERE valid = true")[0]


def update_state():
    return run_query("INSERT INTO state_cache (id) VALUES (1) ON CONFLICT (id) DO UPDATE SET "
                     "  n_pallets = (SELECT COUNT(pallet_multihash) FROM pallet_validated WHERE valid = true) "
                     ", n_votes = (SELECT COALESCE(SUM(n_votes), 0) FROM pallet_validated WHERE valid = true) "
                     ", votes_for = (SELECT COALESCE(SUM(votes_for), 0) FROM pallet_validated WHERE valid = true) "
                     ", votes_against = (SELECT COALESCE(SUM(votes_against), 0) FROM pallet_validated WHERE valid = true)"
                     ", last_processed = %s", [int(time.time())])
